package sample.model.entity.converter;

import javax.persistence.AttributeConverter;

import sample.model.entity.cnst.EntityEnum;


/**
 * EnumとDB値との変換を行う基盤クラス
 *
 * @param <X> DB値の型
 * @param <Y> EntityEnumインターフェースの実装Enumの型
 */
public abstract class EntityEnumConverter<X, Y extends Enum<Y> & EntityEnum<X, Y>> implements AttributeConverter<Y, X> {

	@Override
	public final X convertToDatabaseColumn(Y attribute) {
		return (attribute == null) ? null : attribute.getDbData();
	}

	@Override
	public final Y convertToEntityAttribute(X dbData) {
		return EntityEnum.getEnum(getEnumClass(), dbData);
	}

	/**
	 * EntityEnumインターフェースの実装Enumクラスを取得します。
	 * @return EntityEnumインターフェースの実装Enumクラス
	 */
	abstract Class<Y> getEnumClass();

}
