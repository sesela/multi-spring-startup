package sample.model.entity.converter;

import javax.persistence.Converter;

import sample.model.entity.cnst.PrefectureEnum;


/**
 * 都道府県変換クラス
 *
 */
@Converter
public class PrefectureEnumConverter extends EntityEnumConverter<String, PrefectureEnum> {

	@Override
	final Class<PrefectureEnum> getEnumClass() {
		return PrefectureEnum.class;
	}

}
