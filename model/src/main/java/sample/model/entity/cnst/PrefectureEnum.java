package sample.model.entity.cnst;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 都道府県定数クラス
 *
 */
@AllArgsConstructor
@Getter
public enum PrefectureEnum implements EntityEnum<String, PrefectureEnum> {

	/** 北海道 */
	HOKKAIDO("北海道"),
	/** 青森県 */
	AOMORI("青森県"),
	/** 岩手県 */
	IWATE("岩手県"),
	/** 宮城県 */
	MIYAGI("宮城県"),
	/** 秋田県 */
	AKITA("秋田県"),
	/** 山形県 */
	YAMAGATA("山形県"),
	/** 福島県 */
	FUKUSHIMA("福島県"),
	/** 茨城県 */
	IBARAKI("茨城県"),
	/** 栃木県 */
	TOCHIGI("栃木県"),
	/** 群馬県 */
	GUNMA("群馬県"),
	/** 埼玉県 */
	SAITAMA("埼玉県"),
	/** 千葉県 */
	CHIBA("千葉県"),
	/** 東京都 */
	TOKYO("東京都"),
	/** 神奈川県 */
	KANAGAWA("神奈川県"),
	/** 新潟県 */
	NIIGATA("新潟県"),
	/** 富山県 */
	TOYAMA("富山県"),
	/** 石川県 */
	ISHIKAWA("石川県"),
	/** 福井県 */
	FUKUI("福井県"),
	/** 山梨県 */
	YAMANASHI("山梨県"),
	/** 長野県 */
	NAGANO("長野県"),
	/** 岐阜県 */
	GIFU("岐阜県"),
	/** 静岡県 */
	SHIZUOKA("静岡県"),
	/** 愛知県 */
	AICHI("愛知県"),
	/** 三重県 */
	MIE("三重県"),
	/** 滋賀県 */
	SHIGA("滋賀県"),
	/** 京都府 */
	KYOTO("京都府"),
	/** 大阪府 */
	OSAKA("大阪府"),
	/** 兵庫県 */
	HYOGO("兵庫県"),
	/** 奈良県 */
	NARA("奈良県"),
	/** 和歌山県 */
	WAKAYAMA("和歌山県"),
	/** 鳥取県 */
	TOTTORI("鳥取県"),
	/** 島根県 */
	SHIMANE("島根県"),
	/** 岡山県 */
	OKAYAMA("岡山県"),
	/** 広島県 */
	HIROSHIMA("広島県"),
	/** 山口県 */
	YAMAGUCHI("山口県"),
	/** 徳島県 */
	TOKUSHIMA("徳島県"),
	/** 香川県 */
	KAGAWA("香川県"),
	/** 愛媛県 */
	EHIME("愛媛県"),
	/** 高知県 */
	KOCHI("高知県"),
	/** 福岡県 */
	FUKUOKA("福岡県"),
	/** 佐賀県 */
	SAGA("佐賀県"),
	/** 長崎県 */
	NAGASAKI("長崎県"),
	/** 熊本県 */
	KUMAMOTO("熊本県"),
	/** 大分県 */
	OITA("大分県"),
	/** 宮崎県 */
	MIYAZAKI("宮崎県"),
	/** 鹿児島県 */
	KAGOSHIMA("鹿児島県"),
	/** 沖縄県 */
	OKINAWA("沖縄県");

	/** DB値 */
	private final String dbData;

}
