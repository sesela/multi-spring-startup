package sample.model.entity.embeddable;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sample.model.entity.cnst.PrefectureEnum;

/**
 * 住所クラス
 *
 */
@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {

	/** 都道府県 */
	private PrefectureEnum prefecture;
	/** 市町村 */
	private String city;
	/** 住所 */
	private String addressLine1;
	/** マンション・アパート名等 */
	private String addressLine2;
	/** 郵便番号 */
	private String zipCode;

}
