package sample.common.validator;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import sample.common.validator.impl.CreditCardValidator;

/**
 * クレジットカード書式バリデーションクラスです。
 *
 */
@Target({ FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = CreditCardValidator.class)
@Documented
public @interface CreditCardFormat {

	/**
	 * エラーメッセージコード。デフォルト"{commons.validation.CreditCardFormat.message}"です。
	 * @return エラーメッセージコード
	 */
	String message() default "{commons.validation.CreditCardFormat.message}";

	/**
	 * 制約に対するバリデーションが属するグループ。
	 * @return グループ
	 */
	Class<?>[] groups() default { };

	/**
	 * ペイロード。
	 * @return ペイロード
	 */
	Class<? extends Payload>[] payload() default { };
}
