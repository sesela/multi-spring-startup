package sample.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * SpringBoot起動クラス
 */
@SpringBootApplication(scanBasePackages = {"sample.web", "sample.domain", "sample.common"}) // SUPPRESS CHECKSTYLE For SpringBoot
@EntityScan(basePackages = {"sample.model.entity"})
@EnableJpaRepositories(basePackages = {"sample.domain.repository"})
public class Application {

	/**
	 * メインクラス
	 * @param args 実行引数
	 * @throws Throwable 例外
	 */
	public static void main(String[] args) throws Throwable {
		SpringApplication.run(Application.class, args);
	}

}

