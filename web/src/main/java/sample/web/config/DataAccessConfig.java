package sample.web.config;

import java.util.Optional;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * SpringDataJPA設定クラス
 *
 */
@Configuration
@EnableJpaAuditing(modifyOnCreate = false)
public class DataAccessConfig {

	/**
	 * 監査情報を記録定義メソッド
	 * @return AuditorAware
	 */
	@Bean
	public AuditorAware<String> auditorAware() {
		return () -> {
			String customerCode = null;
			Optional<String> auditor = (customerCode == null) ? Optional.of("system") : Optional.of(customerCode);
			return auditor;
		};
	}

}
